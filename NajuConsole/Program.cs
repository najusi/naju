﻿using NajuLibrary;
using System;

namespace NajuConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Person person = new Person
            {
                LegalName = "나연",
                PreferredName = "Nayeon",
                DateOfBirth = new DateTime(1995, 9, 22)
            };
            Console.WriteLine($"Hello {person.PreferredName}!");
        }
    }
}
