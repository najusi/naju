﻿using System.Linq;
using Xunit;
using NajuLibrary;
namespace NajuTest
{
    public class MinimumCoinTest
    {
        readonly MinimumCoin minimumCoin;
        public MinimumCoinTest()
        {
            var coinValues = new int[] { 1, 5, 10, 25 };
            minimumCoin = new MinimumCoin(CoinSet: coinValues);
        }

        [Fact]
        public void Freebie()
        {
            Assert.Equal(4, 2 + 2);
        }

        [Fact]
        public void GetTrivialCaseOfZeroCoin()
        {
            Assert.Equal(0, minimumCoin.GetCount(0));
        }

        [Fact]
        public void GetTrivialCaseOfOneCoin()
        {
            foreach (int i in minimumCoin.CoinSet)
            {
                Assert.Equal(1, minimumCoin.GetCount(i));
            }
        }

        [Fact]
        public void GetCaseOfTwoCoins()
        {
            if (minimumCoin.CoinSet.Length > 1 && !minimumCoin.CoinSet.Contains(minimumCoin.CoinSet[0] + minimumCoin.CoinSet[1]))
            {
                Assert.Equal(2, minimumCoin.GetCount(minimumCoin.CoinSet[0] + minimumCoin.CoinSet[1]));
            }
        }
    }
}
