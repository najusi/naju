﻿using NajuLibrary;
using System;
using Xunit;

namespace NajuTest
{
    public class PersonTest
    {
        [Fact]
        public void PersonShouldBeCreated()
        {
            Person person = new Person
            {
                LegalName = "나연",
                PreferredName = "Nayeon",
                DateOfBirth = new DateTime(1995, 9, 22)
            };
            Assert.Equal("Nayeon", person.PreferredName);
        }
    }
}
