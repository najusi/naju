﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NajuLibrary
{
    public class MinimumCoin
    {
        public int[] CoinSet { get; set; }

        public MinimumCoin(int[] CoinSet)
        {
            this.CoinSet = CoinSet;
        }

        public int GetCount(int total)
        {
            if (CoinSet.Contains(total))
            {
                return 1;
            }
            int count = 0;
            for (int i = 0; i < CoinSet.Length; i++)
            {
                for (int j = 1; j < CoinSet.Length; j++)
                {
                    if (i != j)
                    {
                        if (CoinSet[i] + CoinSet[j] == total)
                        {
                            return 2;
                        }
                    }
                }
            }
            return count;
        }
    }
}
