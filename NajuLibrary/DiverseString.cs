﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NajuLibrary
{
    public class DiverseString
    {
        public string GetString(int A, int B, int C)
        {
            var result = "";

            var isComplete = false;


            var myList = new Dictionary<string, int>
            {
                { "A", A },
                { "B", B },
                { "C", C }
            }.OrderByDescending(element => element.Value).ToList();

            while (!isComplete)
            {
                if (myList.First().Value == 0 || myList[myList.Count - 1].Value <= 0 && myList[myList.Count - 2].Value <= 0)
                {
                    if (myList.First().Value == 0)
                    {
                        break;
                    }
                }
                // try to find an alternate letter if we have two consecutive of something
                if (result.Length > 1 && result[result.Length - 1] == result[result.Length - 2] && myList[1].Value > 0)
                {
                    result += myList[1].Key.ToString();
                    Console.WriteLine(result);
                    var oldValue = myList[1].Value;
                    var newEntry = new KeyValuePair<string, int>(myList[1].Key, oldValue - 1);
                    myList.Remove(myList[1]);
                    myList.Add(newEntry);
                    myList = myList.OrderByDescending(element => element.Value).ToList();
                }
                // if we can't find an alternate when we have two of something, exit the loop 
                else if (result.Length > 1 && result[result.Length - 1] == result[result.Length - 2] && myList[1].Value == 0)
                {
                    break;
                }
                else
                {
                    result += myList.First().Key.ToString();
                    var oldValue = myList.First().Value;
                    var newEntry = new KeyValuePair<string, int>(myList.First().Key, oldValue - 1);
                    myList.Remove(myList.First());
                    myList.Add(newEntry);
                    myList = myList.OrderByDescending(element => element.Value).ToList();
                }
            }
            return result;
        }
    }
}
