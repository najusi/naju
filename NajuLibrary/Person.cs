﻿using System;

namespace NajuLibrary
{
    public class Person
    {
        public string LegalName { get; set; }
        public string PreferredName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public DateTime? DateOfDeath { get; set; }
    }
}
